// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// Created by Francois Nedelec on 03/09/2014.


#include <iostream>


/// True if terminal supports colors
bool has_colors();

/// print text in red
void print_red(std::ostream&, std::string const&);

/// print text in green
void print_green(std::ostream&, std::string const&);

/// print text in yellow
void print_yellow(std::ostream&, std::string const&);

/// print text in blue
void print_blue(std::ostream&, std::string const&);

/// print text in magenta
void print_magenta(std::ostream&, std::string const&);

/// print text in cyan
void print_cyan(std::ostream&, std::string const&);


